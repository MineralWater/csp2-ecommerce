const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const auth = require("../auth");

//Add to cart
module.exports.addItem = async (reqParams, reqBody, adminData) => {
    let newCart = new Cart({
        totalPrice: reqBody.totalPrice,
        userId: adminData.userId,
        productId: reqParams.productId
    })

    return newCart.save().then((cart, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//Add cart amount
module.exports.addOneItem = async (reqParams, reqBody, adminData)