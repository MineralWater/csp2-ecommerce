const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cart");
const auth = require("../auth")

// Add to Cart
router.post("/addItem", auth.verify, (req, res) => {
	
	const adminData = auth.decode(req.headers.authorization);

	cartController.addToCart(req.params, req.body, adminData).then(resultFromController => res.send(resultFromController));
})

//Add cart amount
router.post("/addOneItem", auth.verify, (req, res) => {
	
	const adminData = auth.decode(req.headers.authorization);

	cartController.addToCart(req.params, req.body, adminData).then(resultFromController => res.send(resultFromController));
})
//Remove one cart
router.post("/removeOneItem", auth.verify, (req, res) => {
	
	const adminData = auth.decode(req.headers.authorization);

	cartController.addToCart(req.params, req.body, adminData).then(resultFromController => res.send(resultFromController));
})
//Remove from cart
router.post("/removeItem", auth.verify, (req, res) => {
	
	const adminData = auth.decode(req.headers.authorization);

	cartController.addToCart(req.params, req.body, adminData).then(resultFromController => res.send(resultFromController));
})


module.exports = router;