const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	quantity: {
		type: Number,
		required: [true, "Total Amount is required"],
		default: 1
	},
	totalPrice: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	productId: {
		type: String,
		required: [true, "Product ID is required"]
	}
})

module.exports = mongoose.model("Cart", cartSchema)